import axios from 'axios'
import { getDrones } from "@/config/api"

// Get All Drones
export const getAllDrones = async () => {
  try {
    const response = await axios.get(`${getDrones}`)
    return response
  } catch (error) {
    return error
  }
}

// Get Drones By Id
export const getDroneById = async (id: number) => {
  try {
    const response = await axios.get(`${getDrones}/${id}`)
    return response
  } catch (error) {
    return error.response
  }
}