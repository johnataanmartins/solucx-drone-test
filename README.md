# Drones

This is a test for a front-end developer job. This project is consuming an API from Solucx to present all the drones, on the main page we have the possibility to filter drones by id, name, current fly, and status, and the user also has the possibility of seeing a drone on a detail page.

### Technologies
```
- Vue Framework
- Vue Bootstrap Components
- Axios for HTTP requests
```

### Project is running on the following address:

- http://front-end-cloud-test-projects.rj.r.appspot.com/

### Before run this project install the dependencies running the following command:
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```